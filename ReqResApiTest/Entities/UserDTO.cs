﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class UserDTO
    {
        [JsonProperty("data")]
        public Data data { get; set; }

        [JsonProperty("support")]
        public Support support { get; set; }
    }
}
