﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class SingleResourceDTO
    {
        [JsonProperty("data")]
        public Datum data { get; set; }

        [JsonProperty("support")]
        public Support support { get; set; }
    }
}
