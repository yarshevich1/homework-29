﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class UserInfoDTO
    {
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("job")]
        public string job { get; set; }
    }
}
