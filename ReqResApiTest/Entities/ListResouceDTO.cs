﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class ListResouceDTO
    {
        [JsonProperty("page")]
        public int page { get; set; }

        [JsonProperty("per_page")]
        public int per_page { get; set; }

        [JsonProperty("total")]
        public int total { get; set; }

        [JsonProperty("total_pages")]
        public int total_pages { get; set; }

        [JsonProperty("data")]
        public List<Datum> data { get; set; }

        [JsonProperty("support")]
        public Support support { get; set; }
    }

    public class Datum
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("year")]
        public int year { get; set; }

        [JsonProperty("color")]
        public string color { get; set; }

        [JsonProperty("pantone_value")]
        public string pantone_value { get; set; }
    }

}
