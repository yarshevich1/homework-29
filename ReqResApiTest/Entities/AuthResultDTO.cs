﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class AuthResultDTO
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("token")]
        public string token { get; set; }

        [JsonProperty("error")]
        public string error { get; set; }
    }
}
