﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class UsersListDTO
    {
        [JsonProperty("page")]
        public int page { get; set; }

        [JsonProperty("per_page")]
        public int per_page { get; set; }

        [JsonProperty("total")]
        public int total { get; set; }

        [JsonProperty("total_pages")]
        public int total_pages { get; set; }

        [JsonProperty("data")]
        public List<Data> data { get; set; }

        [JsonProperty("support")]
        public Support support { get; set; }
    }

    public class Support
    {
        [JsonProperty("url")]
        public string url { get; set; }

        [JsonProperty("text")]
        public string text { get; set; }
    }
    public class Data
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("first_name")]
        public string first_name { get; set; }

        [JsonProperty("last_name")]
        public string last_name { get; set; }

        [JsonProperty("avatar")]
        public string avatar { get; set; }
    }
}
