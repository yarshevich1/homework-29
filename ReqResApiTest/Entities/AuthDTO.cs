﻿using Newtonsoft.Json;

namespace ReqResApiTest.Entities
{
    internal class AuthDTO
    {
        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }
    }
}
