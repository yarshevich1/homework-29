﻿using ReqResApiTest.Entities;
using RestSharp;
using System.Text.Json;

namespace ReqResApiTest.Client
{
    internal class ReqResService
    {
        public static RestClient ResClient { get; set; }
        public static RestResponse Response { get; set; }
        public static RestRequest Request { get; set; }

        public static RestRequest SetRequest(string? url, Method method)
        {
            return new RestRequest(url, method);
        }

        public static RestResponse SendRequest(string? url, Method method)
        {
            Response = ResClient.ExecuteAsync(SetRequest(url, method)).Result;
            return Response;
        }

        public static RestResponse SendRequest(RestRequest request)
        {
            Response = ResClient.ExecuteAsync(request).Result;
            return Response;
        }

        public static UsersListDTO GetUsersList(int page)
        {
            var request = SetRequest($"/api/users?page={page}", Method.Get);
            var responseUsersList = JsonSerializer.Deserialize<UsersListDTO>(SendRequest(request).Content);
            return responseUsersList;
        }

        public static UsersListDTO GetUsersListDelay(int page)
        {
            var request = SetRequest($"/api/users?delay={page}", Method.Get);
            var responseUsersList = JsonSerializer.Deserialize<UsersListDTO>(SendRequest(request).Content);
            return responseUsersList;
        }

        public static UserDTO GetSingleUser(int id)
        {
            var request = SetRequest($"/api/users/{id}", Method.Get);
            var response = JsonSerializer.Deserialize<UserDTO>(SendRequest(request).Content);
            return response;
        }

        public static ListResouceDTO GetListResources()
        {
            var request = SetRequest("/api/unknown", Method.Get);
            var response = JsonSerializer.Deserialize<ListResouceDTO>(SendRequest(request).Content);
            return response;
        }

        public static SingleResourceDTO GetSingleResource(int id)
        {
            var request = SetRequest($"/api/unknown/{id}", Method.Get);
            var response = JsonSerializer.Deserialize<SingleResourceDTO>(SendRequest(request).Content);
            return response;
        }

        public static UserInfoDTO CreateUser(UserInfoDTO userInfo)
        {
            var request = SetRequest("/api/users", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(userInfo);
            var response = JsonSerializer.Deserialize<UserInfoDTO>(SendRequest(request).Content);
            return response;
        }

        public static UserInfoDTO UpdateUser(UserInfoDTO userInfo, int id, Method method)
        {
            var request = SetRequest($"/api/users/{id}", method);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(userInfo);
            var response = JsonSerializer.Deserialize<UserInfoDTO>(SendRequest(request).Content);
            return response;
        }

        public static RestResponse DeleteUser(int id)
        {
            var request = SetRequest($"/api/users/{id}", Method.Delete);
            request.RequestFormat = DataFormat.Json;
            var response = SendRequest(request);
            return response;
        }

        public static AuthResultDTO RegisterUser(AuthDTO auth)
        {
            var request = SetRequest($"/api/register", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(auth);
            var response = JsonSerializer.Deserialize<AuthResultDTO>(SendRequest(request).Content);
            return response;
        }

        public static AuthResultDTO LoginUser(AuthDTO auth)
        {
            var request = SetRequest($"/api/login", Method.Post);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(auth);
            var response = JsonSerializer.Deserialize<AuthResultDTO>(SendRequest(request).Content);
            return response;
        }


    }
}
