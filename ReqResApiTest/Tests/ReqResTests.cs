﻿using ReqResApiTest.Client;
using ReqResApiTest.Entities;
using RestSharp;

namespace ReqResApiTest.Tests
{
    internal class ReqResTests : BaseTest
    {
        [Test]
        public void GetUsersList()
        {
            var result = ReqResService.GetUsersList(2);
            Assert.Multiple((() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.page, Is.EqualTo(2));
            }));
        }

        [Test]
        public void GetUsersListDelay()
        {
            var result = ReqResService.GetUsersListDelay(3);
            Assert.Multiple((() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.page, Is.EqualTo(1));
                Assert.That(result.total, Is.EqualTo(12));
            }));
        }

        [Test]
        public void GetSingleUser()
        {
            var result = ReqResService.GetSingleUser(2);
            Assert.Multiple((() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.data.id, Is.EqualTo(2));
                Assert.That(result.data.email, Is.EqualTo("janet.weaver@reqres.in"));
            }));
        }

        [Test]
        public void SingleUserNotFound()
        {
            var result = ReqResService.GetSingleUser(23);
            Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(404));
        }

        [Test]
        public void GetListResource()
        {
            var result = ReqResService.GetListResources();
            Assert.Multiple((() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.page, Is.EqualTo(1));
                Assert.That(result.total, Is.EqualTo(12));
                Assert.That(result.data[0].color, Is.EqualTo("#98B2D1"));
            }));
        }

        [Test]
        public void GetSingleResource()
        {
            var result = ReqResService.GetSingleResource(2);
            Assert.Multiple((() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.data.color, Is.EqualTo("#C74375"));
                Assert.That(result.data.pantone_value, Is.EqualTo("17-2031"));
            }));
        }

        [Test]
        public void SingleResourceNotFound()
        {
            var result = ReqResService.GetSingleResource(23);
            Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(404));
        }

        [Test]
        public void CreateUser()
        {
            var result = ReqResService.CreateUser(new UserInfoDTO
            {
                name = "Alex",
                job = "manager"
            });

            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(201));
                Assert.That(result.name, Is.EqualTo("Alex"));
            });
        }

        [Test]
        public void UpdateUserWithPut()
        {
            var result = ReqResService.UpdateUser(new UserInfoDTO
            {
                name = "Alex",
                job = "driver car"
            }, 188, Method.Put);

            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.job, Is.EqualTo("driver car"));
                Assert.That(result.name, Is.EqualTo("Alex"));
            });
        }

        [Test]
        public void UpdateUserWithPath()
        {
            var result = ReqResService.UpdateUser(new UserInfoDTO
            {
                name = "Alex",
                job = "pilot"
            }, 188, Method.Patch);

            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(result.job, Is.EqualTo("pilot"));
                Assert.That(result.name, Is.EqualTo("Alex"));
            });
        }

        [Test]
        public void DeleteUser()
        {
            var result = ReqResService.DeleteUser(188);
            Assert.That((int)result.StatusCode, Is.EqualTo(204));
        }

        [Test]
        public void RegisterUserSuccessful()
        {
            var request = ReqResService.RegisterUser(new AuthDTO
            {
                email = "eve.holt@reqres.in",
                password = "pistol"
            });
            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(request.id, Is.EqualTo(4));
                Assert.That(request.token, Is.EqualTo("QpwL5tke4Pnpja7X4"));
            });
        }

        [Test]
        public void RegisterUserUnsuccessful()
        {
            var request = ReqResService.RegisterUser(new AuthDTO
            {
                email = "sydney@fife"
            });
            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(400));
                Assert.That(request.error, Is.EqualTo("Missing password"));
            });
        }

        [Test]
        public void LoginUserSuccessful()
        {
            var request = ReqResService.LoginUser(new AuthDTO
            {
                email = "eve.holt@reqres.in",
                password = "cityslicka"
            });
            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(200));
                Assert.That(request.token, Is.EqualTo("QpwL5tke4Pnpja7X4"));
            });
        }

        [Test]
        public void LoginUserUnsuccessful()
        {
            var request = ReqResService.RegisterUser(new AuthDTO
            {
                email = "peter@klaven"
            });
            Assert.Multiple(() =>
            {
                Assert.That((int)ReqResService.Response.StatusCode, Is.EqualTo(400));
                Assert.That(request.error, Is.EqualTo("Missing password"));
            });
        }
    }
}
