﻿using ReqResApiTest.Client;

namespace ReqResApiTest.Tests
{
    internal class BaseTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ReqResService.ResClient = new(new Uri(Configuration.BaseURL));
        }

        [TearDown]
        public void TearDown()
        {

        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            ReqResService.ResClient.Dispose();
        }
    }
}
